package com.npegane.note;


import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static android.content.Context.MODE_PRIVATE;
import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

public class PasswordDialog extends DialogFragment {
    private EditText pwField;
    private Button btnOk;
    private Button btnCancel;
    private SQLiteDatabase db;
    private int noteId;
    private String operationType;
    private TextView lbl_pw_title;
    public PasswordDialog(){

    }
    public static PasswordDialog newInstance(String title){
        PasswordDialog dialog = new PasswordDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        //dialog.setArguments(args);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.password_fragment, container);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        pwField = view.findViewById(R.id.txt_set_pw);
        btnOk = view.findViewById(R.id.btnOk);
        btnCancel = view.findViewById(R.id.btnCancel);
        lbl_pw_title = view.findViewById(R.id.lbl_pw_title);

        Bundle bundle = getArguments();
        operationType = bundle.getString("operationType");
        noteId = bundle.getInt("noteId");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        if(operationType == "set"){
            lbl_pw_title.setText("Choisir un mot de passe");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String pw = pwField.getText().toString();
                    ((MainActivity) getActivity()).addPassword(pw);
                    dismiss();
                }
            });
        }
        else if (operationType == "check"){
            lbl_pw_title.setText("Rentrez le mot de passe");
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String pw = pwField.getText().toString();
                    System.out.println(noteId);
                    if(((NoteListActivity) getActivity()).checkPassword(pw, noteId)){
                        Intent intent = new Intent(((NoteListActivity) getActivity()).getApplicationContext(), MainActivity.class);
                        intent.putExtra("NOTE_ID", noteId);
                        ((NoteListActivity) getActivity()).startActivity(intent);
                        dismiss();
                    } else {
                        pwField.setText("");
                    }

                }
            });
        } else if (operationType == "delete"){
            String pw = pwField.getText().toString();
            lbl_pw_title.setText("Rentrez le mot de passe");
            System.out.println(((NoteListActivity) getActivity()).checkPassword(pw, noteId));

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String pw = pwField.getText().toString();
                    System.out.println(noteId);
                    if(((NoteListActivity) getActivity()).checkPassword(pw, noteId)){
                        System.out.println(noteId);
                        ((NoteListActivity) getActivity()).deleteNote(noteId);
                        dismiss();
                    } else {
                        pwField.setText("");
                    }
                }
            });
        }


        //String title = getArguments().getString("title", "Choisir un Mot de passe");
        //getDialog().setTitle(title);
        pwField.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void initDB(){


    }
}
