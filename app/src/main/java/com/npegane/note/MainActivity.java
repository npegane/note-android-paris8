package com.npegane.note;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private final String DBNAME = "DBNOTE";
    SQLiteDatabase db;
    static int noteId;
    TextView noteTitle;
    EditText titleEdit;
    TextView noteContent;
    EditText contentEdit;
    ImageButton btnReturn;
    Button btnCancel;
    ImageButton btnPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_note);
        noteTitle = findViewById(R.id.noteTitle);
        noteContent = findViewById(R.id.noteContent);
        contentEdit = findViewById(R.id.noteContentEdit);
        titleEdit = findViewById(R.id.noteTitleEdit);
        btnReturn = findViewById(R.id.btnReturn);
        btnPassword = findViewById(R.id.btnPassword);


        btnCancel = findViewById(R.id.btnCancel);
        initDB();
        Bundle extra = getIntent().getExtras();
        noteId = extra.getInt("NOTE_ID");
        loadNote(noteId);

        noteTitle.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v){
                titleEdit.setText(noteTitle.getText());
                noteTitle.setVisibility(View.GONE);
                titleEdit.setVisibility(View.VISIBLE);
                return true;
            }
        });

        noteContent.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(getBaseContext(), "Long Clicked", Toast.LENGTH_SHORT).show();
                contentEdit.setText(noteContent.getText());
                noteContent.setVisibility(View.GONE);
                contentEdit.setVisibility(View.VISIBLE);
                return true;
            }
        });

        btnPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                PasswordDialog passDial = PasswordDialog.newInstance("Choisir un mot de passe");
                Bundle args = new Bundle();
                args.putInt("noteId", MainActivity.noteId);
                args.putString("operationType", "set");
                passDial.setArguments(args);
                passDial.show(fm, "choose_password");
            }
        });
        /*btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noteContent.setText(contentEdit.getText());
                contentEdit.setVisibility(View.GONE);
                noteContent.setVisibility(View.VISIBLE);

                noteTitle.setText(titleEdit.getText());
                titleEdit.setVisibility(View.GONE);
                noteTitle.setVisibility(View.VISIBLE);

                String newTitle = noteTitle.getText().toString();
                String newContent = noteContent.getText().toString();
                db.execSQL("UPDATE NOTE SET title = '"+ newTitle +"' , content = '" + newContent + "' WHERE id = '" + noteId +"'");
            }
        });*/

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noteContent.setText(contentEdit.getText());
                contentEdit.setVisibility(View.GONE);
                noteContent.setVisibility(View.VISIBLE);

                noteTitle.setText(titleEdit.getText());
                titleEdit.setVisibility(View.GONE);
                noteTitle.setVisibility(View.VISIBLE);

                String newTitle = noteTitle.getText().toString();
                String newContent = noteContent.getText().toString();
                db.execSQL("UPDATE NOTE SET title = '"+ newTitle +"' , content = '" + newContent + "' WHERE id = '" + noteId +"'");
                finishThisActivity();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                finishThisActivity();
            }
        });
    }

    public void finishThisActivity(){
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addPassword(String pw){
        db.execSQL("UPDATE NOTE SET password = '"+pw+"' WHERE id= '" + noteId +"'");
    }

    public void checkPassword(String toCheck){
        db.execSQL("SELECT password FROM NOTE WHERE id = '" + noteId + "' ");
    }

    public void initDB(){
        db = openOrCreateDatabase(DBNAME, MODE_PRIVATE, null);
        /*adb.execSQL("CREATE TABLE IF NOT EXISTS NOTE (" +
                    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "title VARCHAR(200) NOT NULL," +
                    "content VARCHAR(200) NOT NULL," +
                    "password VARCHAR(200)"+
                ")");*/
        //db.execSQL("INSERT INTO NOTE (title, content) values('test','testContent')");
    }

    public void loadNote(int id){
        String strQuery = "SELECT * FROM NOTE WHERE id = '"+id+"'";
        System.out.println(strQuery);
        Cursor cur = db.rawQuery("SELECT * FROM NOTE WHERE id = '"+id+"';", null);
        while(cur.moveToNext()){
            this.titleEdit.setText(cur.getString(1));
            this.noteTitle.setText(cur.getString(1));
            this.contentEdit.setText(cur.getString(2));
            this.noteContent.setText(cur.getString(2));
        }
        cur.close();
    }
}
