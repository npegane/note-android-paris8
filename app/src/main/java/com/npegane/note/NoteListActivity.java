package com.npegane.note;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NoteListActivity extends AppCompatActivity {
    private final String DBNAME = "DBNOTE";
    SQLiteDatabase db;
    FloatingActionButton btnAdd;
    LinearLayout mainLinearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);
        btnAdd = findViewById(R.id.btnAdd);
        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickAddNote();
            }
        });
        initDB();
        loadNotes();
    }

    @Override
    public void onResume(){
        super.onResume();
        System.out.println("On resume");
        cleanNotes();
        loadNotes();
    }

    public void initDB(){
        db = openOrCreateDatabase(DBNAME, MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS NOTE (" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "title VARCHAR(200) NOT NULL," +
                "content VARCHAR(200) NOT NULL," +
                "password VARCHAR(200)"+
                ")");
    }

    public void deleteNote(int noteId){
        db.execSQL("DELETE FROM NOTE WHERE id = '"+ noteId + "'");
        cleanNotes();
        loadNotes();
    }

    public boolean checkPassword(String toCheck, int noteId){
        String strQuery = "SELECT * FROM NOTE WHERE id = '"+noteId+"'";
        System.out.println(strQuery);
        Cursor cur = db.rawQuery(strQuery, null);
        String real_pw = "";
        System.out.println(cur.moveToFirst());
        if(cur != null && cur.moveToFirst()){
            real_pw = cur.getString(cur.getColumnIndex("password"));

            System.out.println(cur.getColumnCount());
            System.out.println(cur.getString(0));

            System.out.println(noteId);
            System.out.println(real_pw);
            System.out.println(real_pw.equals(toCheck));cur.close();
            return real_pw.equals(toCheck);
        }
        return false;

    }

    public void cleanNotes(){
        mainLinearLayout.removeAllViews();
    }

    public void loadNotes(){
        Cursor cur = db.rawQuery("SELECT * FROM NOTE ", null);
        while(cur.moveToNext()){
            LinearLayout ll_note = new LinearLayout(this.getBaseContext());
            ll_note.setLayoutParams( new LinearLayout.LayoutParams(400, LinearLayout.LayoutParams.MATCH_PARENT));
            ll_note.setOrientation(LinearLayout.HORIZONTAL);

            final LinearLayout ll_note_content = new LinearLayout(this.getBaseContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 2);
            layoutParams.setMargins(0, 5, 0, 5);
            ll_note_content.setLayoutParams(layoutParams);
            ll_note_content.setOrientation(LinearLayout.VERTICAL);

            TextView tv_title = new TextView(this.getBaseContext());
            TextView tv_content = new TextView(this.getBaseContext());
            final int noteId = cur.getInt(0);

            String txtTitle = cur.getString(1);
            if(txtTitle.length()>20){
                txtTitle = txtTitle.substring(0,15) + "...";
            }
            tv_title.setText(txtTitle);



            ImageView lock = new ImageView(getBaseContext());
            ll_note.addView(lock);
            float lockWidth = getResources().getDimension(R.dimen.lock_width);
            LinearLayout.LayoutParams lockLayoutParams = new LinearLayout.LayoutParams((int)lockWidth, LinearLayout.LayoutParams.MATCH_PARENT, 2);
            lock.setLayoutParams(lockLayoutParams);
            if(cur.getString(3) != null){
                lock.setImageResource(android.R.drawable.ic_lock_lock);
                tv_content.setText("VERROUILLÉ");
                lock.setColorFilter(ContextCompat.getColor(this.getBaseContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                ll_note_content.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                        PasswordDialog passDial = PasswordDialog.newInstance("Choisir un mot de passe");
                        Bundle args = new Bundle();
                        args.putInt("noteId", noteId);
                        args.putString("operationType", "check");
                        passDial.setArguments(args);
                        passDial.show(fm, "check_password");
                    }
                });
            } else {
                String txtContent = cur.getString(2);
                if(txtContent.length()>20){
                    txtContent = txtContent.substring(0,15) + "...";
                }
                tv_content.setText(txtContent);
                ll_note_content.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("NOTE_ID", noteId);
                        startActivity(intent);
                    }
                });
            }
            tv_title.setTextColor(Color.BLACK);
            tv_content.setTextColor(Color.BLACK);
            ll_note_content.addView(tv_title);
            ll_note_content.addView(tv_content);
            ll_note_content.setClickable(true);


            ll_note_content.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                        ll_note_content.setBackgroundColor(getResources().getColor(R.color.blueSelect));
                        return false;
                    }
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP || motionEvent.getAction() == MotionEvent.ACTION_CANCEL){
                        ll_note_content.setBackgroundColor(Color.WHITE);
                        return false;
                    }
                    return false;
                }
            });

            Button btnDelete = new Button(getBaseContext());
            btnDelete.setBackgroundResource(R.drawable.delete);
            LinearLayout.LayoutParams lp_btn = new LinearLayout.LayoutParams((int)getResources().getDimension(R.dimen.lock_width),45,2);
            lp_btn.setMargins(0,20,0,0);
            btnDelete.setLayoutParams(lp_btn);
            if(cur.getString(3) != null){
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                        PasswordDialog passDial = PasswordDialog.newInstance("Choisir un mot de passe");
                        Bundle args = new Bundle();
                        args.putInt("noteId", noteId);
                        args.putString("operationType", "delete");
                        passDial.setArguments(args);
                        passDial.show(fm, "check_password");
                    }
                });
            } else {
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteNote(noteId);
                    }
                });
            }

            //btnDelete.setWidth(5);
            //btnDelete.setHeight(5);

            ll_note.addView(ll_note_content);
            ll_note.addView(btnDelete);


            mainLinearLayout.addView(ll_note);
        }
    }

    public void clickAddNote(){
        Intent intent = new Intent(this, NewNote.class);
        startActivity(intent);
    }
}
