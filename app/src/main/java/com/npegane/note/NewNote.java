package com.npegane.note;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class NewNote extends AppCompatActivity {
    private final String DBNAME = "DBNOTE";
    SQLiteDatabase db;
    TextView noteTitle;
    EditText titleEdit;
    TextView noteContent;
    EditText contentEdit;
    ImageButton btnSave;
    Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        noteTitle = findViewById(R.id.noteTitle);
        noteContent = findViewById(R.id.noteContent);
        contentEdit = findViewById(R.id.noteContentEdit);
        titleEdit = findViewById(R.id.noteTitleEdit);
        btnSave = findViewById(R.id.btnSave);
        btnCancel = findViewById(R.id.btnCancel);
        initDB();
        titleEdit.setFocusable(true);
        titleEdit.setFocusableInTouchMode(true);
        contentEdit.setFocusable(true);
        contentEdit.setFocusableInTouchMode(true);

        noteTitle.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v){
                titleEdit.setText(noteTitle.getText());
                noteTitle.setVisibility(View.GONE);
                titleEdit.setVisibility(View.VISIBLE);
                titleEdit.requestFocus();
                return true;
            }
        });

        noteContent.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(getBaseContext(), "Long Clicked", Toast.LENGTH_SHORT).show();
                contentEdit.setText(noteContent.getText());
                noteContent.setVisibility(View.GONE);
                contentEdit.setVisibility(View.VISIBLE);
                contentEdit.requestFocus();
                //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                return true;
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noteTitle.setText(titleEdit.getText());
                titleEdit.setVisibility(View.GONE);
                noteTitle.setVisibility(View.VISIBLE);
                noteContent.setText(contentEdit.getText());
                contentEdit.setVisibility(View.GONE);
                noteContent.setVisibility(View.VISIBLE);
                addNote();
                finishThisActivity();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishThisActivity();
            }
        });
    }

    public void addNote(){
        String title = noteTitle.getText().toString() ;
        String content = noteContent.getText().toString();
        System.out.println("title:"+title+" content:"+content);
        db.execSQL("INSERT INTO NOTE (title, content) values('" + title + "','" + content + "')");
    }
    private void finishThisActivity(){
        this.finish();
    }
    public void initDB(){
        db = openOrCreateDatabase(DBNAME, MODE_PRIVATE, null);
    }
}
